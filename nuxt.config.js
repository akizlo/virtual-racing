const pkg = require('./package');

module.exports = {
  mode: 'universal',

  env: {
    production: process.env.NODE_ENV === 'production',
    stripeKey: process.env.NODE_ENV === 'production' ? 'pk_live_dsjRCZzdWacFKPFhMs35wCqJ' : 'pk_test_0O0KwQKWVXRSb8Op3JLl48E4',
  },
  /*
  ** Headers of the page
  */
  head: {
    title: pkg.name,
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Final Surge Virtual Racing Events' },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/icon.png' }],
  },

  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },

  /*
  ** Global CSS
  */
  css: [
    'element-ui/lib/theme-chalk/index.css',
    '@assets/scss/bootstrap.scss',
    '@assets/scss/global.scss',
    '@assets/scss/flags.scss',
    '@assets/scss/font-awesome.scss',
    '@assets/scss/element-ui.scss',
  ],

  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '@/plugins/element-ui',
    '@/plugins/axios',
    '@/plugins/vee-validate',
  ],

  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
  ],
  /*
  ** Axios module configuration
  */
  axios: {
    baseURL: `http://${process.env.HOST || '127.0.0.1'}:${process.env.PORT || 80}`,
    browserBaseURL: '/',
  },

  router: {
    trailingSlash: true,
  },

  /*
  ** Build configuration
  */
  build: {
    transpile: [
      /^element-ui/,
      'vee-validate/dist/rules',
    ],
    extractCSS: process.env.NODE_ENV === 'production',
    extend(config, ctx) {
      // Run ESLint on save
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/,
        });
      }
    },
  },
};
