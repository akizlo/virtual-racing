import Vue from 'vue'
import Router from 'vue-router'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _8a80089e = () => interopDefault(import('..\\pages\\contact-us.vue' /* webpackChunkName: "pages_contact-us" */))
const _2e180870 = () => interopDefault(import('..\\pages\\race\\_provider\\_race\\_event\\index.vue' /* webpackChunkName: "pages_race__provider__race__event_index" */))
const _7df23060 = () => interopDefault(import('..\\pages\\race\\_provider\\_race\\_event\\details.vue' /* webpackChunkName: "pages_race__provider__race__event_details" */))
const _efaa2816 = () => interopDefault(import('..\\pages\\race\\_provider\\_race\\_event\\register.vue' /* webpackChunkName: "pages_race__provider__race__event_register" */))
const _4fbeffd8 = () => interopDefault(import('..\\pages\\race\\_provider\\_race\\_event\\results.vue' /* webpackChunkName: "pages_race__provider__race__event_results" */))
const _1b8b867c = () => interopDefault(import('..\\pages\\index.vue' /* webpackChunkName: "pages_index" */))

// TODO: remove in Nuxt 3
const emptyFn = () => {}
const originalPush = Router.prototype.push
Router.prototype.push = function push (location, onComplete = emptyFn, onAbort) {
  return originalPush.call(this, location, onComplete, onAbort)
}

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: decodeURI('/'),
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/contact-us/",
    component: _8a80089e,
    pathToRegexpOptions: {"strict":true},
    name: "contact-us"
  }, {
    path: "/race/:provider?/:race?/:event?/",
    component: _2e180870,
    pathToRegexpOptions: {"strict":true},
    name: "race-provider-race-event"
  }, {
    path: "/race/:provider?/:race?/:event?/details/",
    component: _7df23060,
    pathToRegexpOptions: {"strict":true},
    name: "race-provider-race-event-details"
  }, {
    path: "/race/:provider?/:race?/:event?/register/",
    component: _efaa2816,
    pathToRegexpOptions: {"strict":true},
    name: "race-provider-race-event-register"
  }, {
    path: "/race/:provider?/:race?/:event?/results/",
    component: _4fbeffd8,
    pathToRegexpOptions: {"strict":true},
    name: "race-provider-race-event-results"
  }, {
    path: "/",
    component: _1b8b867c,
    pathToRegexpOptions: {"strict":true},
    name: "index"
  }],

  fallback: false
}

export function createRouter () {
  return new Router(routerOptions)
}
