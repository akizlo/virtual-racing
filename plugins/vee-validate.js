import { extend } from 'vee-validate';
import { required, email, numeric, min } from 'vee-validate/dist/rules';

extend('required', required);
extend('email', email);
extend('numeric', numeric);
extend('min', min);

extend('range', {
  params: ['min', 'max'],
  validate: (value, { min, max }) => {
    let valid = true;
    if (min !== undefined && value < min) {
      valid = false;
    }
    if (max !== undefined && value > max) {
      valid = false;
    }
    return valid;
  },
});

extend('date', {
  params: ['year', 'month'],
  validate: (value, { year, month }) => {
    let isValid = true;
    if (!(value >= 1 && value <= 31)) {
      isValid = false;
    } else if (year && month) {
      const initialDate = parseInt(value);
      const initialMonth = parseInt(month) - 1;
      const initialYear = parseInt(year);
      const date = new Date();
      date.setFullYear(initialYear);
      date.setMonth(initialMonth);
      date.setDate(initialDate);
      if (date.getFullYear() !== initialYear || date.getMonth() !== initialMonth || date.getDate() !== initialDate) {
        isValid = false;
      }
    }
    return isValid;
  },
  message: 'Date is invalid.',
});

extend('same', {
  params: ['target'],
  validate: (value, { target }) => {
    return !(target && value !== target);
  },
  message: 'Passwords do not match',
});

extend('password', {
  validate: (value) => {
    const reg = new RegExp('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{7,})');
    return reg.test(value);
  },
  message: 'Must be 7-15 characters long, must include: upper-case letter, lower-case letter, number.',
});
