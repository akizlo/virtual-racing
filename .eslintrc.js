module.exports = {
  root: true,
  env: {
    browser: true,
    node: true
  },
  parserOptions: {
    parser: 'babel-eslint'
  },
  extends: [
    '@nuxtjs',
  ],
  // add your custom rules here
  rules: {
    "semi": ["error", "always"],
    "skipBlankLines": true,
    "indent": "off",
    "no-console": "off",
    "space-before-function-paren": ["error", "never"],
    "comma-dangle": ["error", "always-multiline"],
    "vue/require-default-prop": "off",
    "vue/require-prop-types": "off",
    "vue/html-indent": ["error", 2, {
      "attribute": 1,
      "baseIndent": 1,
      "closeBracket": 0,
      "alignAttributesVertically": true,
      "ignores": []
    }],
    "vue/script-indent": ["error", 2, {
      "baseIndent": 1,
      "switchCase": 0,
      "ignores": []
    }],
  }
};
