const store = {
  state() {
    return {
      authenticatedUser: null,
      authenticatedUserProfile: null,
      authenticating: false,
      timezones: [],
      countries: [],
      states: [],
      pendingRedirect: null,
    };
  },
  actions: {
    async fetchTimezones({ state, commit }) {
      if (state.timezones.length) {
        return state.timezones;
      } else {
        const result = (await this.$axios.get(
          '/api/data/TimeZone',
        )).data;
        commit('setTimezones', result);
        return result;
      }
    },
    async fetchStates({ state, commit }, country) {
      return (await this.$axios.get(
        `/api/data/StateList?country_key=${country}`,
      )).data;
    },
    async fetchCountries({ state, commit }) {
      if (state.countries.length) {
        return state.countries;
      } else {
        const result = (await this.$axios.get(
          '/api/data/CountryList',
        )).data;
        commit('setCountries', result);
        return result;
      }
    },
    async registerUser({ commit, dispatch }, data) {
      commit('setAuthenticatingState', true);
      const result = (await this.$axios.post(
        '/api/register',
        {
          first_name: data.firstName,
          last_name: data.lastName,
          email: data.email,
          time_zone: data.timezone,
          password: data.password,
        },
      )).data;
      if (result.success) {
        return await dispatch('authenticateUser', data);
      } else {
        commit('setAuthenticatingState', false);
        return result;
      }
    },
    async resetPassword({ commit, dispatch }, data) {
      commit('setAuthenticatingState', true);
      await this.$axios.post(
        '/api/data/PasswordReset',
        data,
      );
      commit('setAuthenticatingState', false);
    },
    async authenticateUser({ commit, dispatch }, data) {
      commit('setAuthenticatingState', true);
      const result = (await this.$axios.post(
        '/api/login',
        data,
      )).data;
      if (result.data && result.data.token) {
        const user = await dispatch('loadCurrentUser');
        commit('setAuthenticatingState', false);
        return { data: user, success: true };
      } else {
        commit('setAuthenticatingState', false);
        return result;
      }
    },
    async loadCurrentUser({ commit }) {
      const result = (await this.$axios.get(
        '/api/data/Settings'
      )).data;
      if (result) {
        commit('setAuthenticatedUser', result);
      }
      return result;
    },
  },
  mutations: {
    setPendingRedirect(state, data) {
      state.pendingRedirect = data;
    },
    setTimezones(state, data) {
      state.timezones = data;
    },
    setCountries(state, data) {
      state.countries = data;
    },
    setStates(state, data) {
      state.states = data;
    },
    setAuthenticatingState(state, data) {
      state.authenticating = data;
    },
    setAuthenticatedUser(state, data) {
      state.authenticatedUser = data;
    },
    setAuthenticatedUserProfile(state, data) {
      state.authenticatedUserProfile = data;
    },
  },
};

export default store;
