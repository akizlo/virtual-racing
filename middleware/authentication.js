export default async function({ meta, route, store, redirect, $axios }) {
  const settings = (await $axios.get('/api/data/settings')).data;
  if (settings) {
    store.commit('setAuthenticatedUser', settings);
    const profile = (await $axios.get(`/api/data/Profile?user_key=${settings.user_key}`)).data;
    store.commit('setAuthenticatedUserProfile', profile);
  }
  if (route.meta && route.meta.length) {
    const lastMeta = route.meta[route.meta.length - 1];
    if (lastMeta.private && !settings) {
      redirect('/');
    }
  }
}
