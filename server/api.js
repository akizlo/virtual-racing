const Cookies = require('cookies');
const express = require('express');
const axios = require('axios');
const bodyParser = require('body-parser');
const router = express.Router();

const handleAuthHeaders = function(request, config) {
  const cookies = new Cookies(request);
  const sessionId = cookies.get('session_id');

  const authHeaders = {};
  if (sessionId) {
    authHeaders.Authorization = `Bearer ${sessionId}`;
  }

  const resultConfig = config || ({});
  const headerConfig = resultConfig.headers || ({});

  resultConfig.headers = Object.assign(headerConfig, authHeaders);
  return resultConfig;
};

const httpClient = axios.create({
  baseURL: 'https://mobileapi.finalsurge.com/',
  headers: {
    'client-secret': 'U5ZR_N25H4U7wVKWubtZ5*Ny4X+2nJ',
    'client-id': 'd99d0dc7-60a2-42b3-bc6a-dc09935521b9',
  },
});

httpClient.interceptors.response.use(
  response => response,
  (error) => {
    return Promise.resolve(error.response);
  }
);

router.use('/data/:service', bodyParser.json(), async function(req, res, next) {
  const method = req.method === 'POST' ? 'post' : 'get';
  const index = req.originalUrl.indexOf('?');
  const query = (index < 0) ? '' : req.originalUrl.slice(index);
  console.log(`API - ${method} - ${req.params.service + query}`);
  if (req.params.service) {
    let results;
    if (method === 'post') {
      results = await httpClient[method](
        req.params.service + query,
        req.body,
        handleAuthHeaders(req),
      );
    } else {
      results = await httpClient[method](
        req.params.service + query,
        handleAuthHeaders(req),
      );
    }
    res.json(results.data.data);
  } else {
    next();
  }
});

router.post('/registerRace', bodyParser.json(), async function(req, res) {
  const results = await httpClient.post(
    `RaceRegister?is_test=${process.env.NODE_ENV === 'development'}`,
    req.body,
    handleAuthHeaders(req, {
      params: req.query,
    }),
  );
  res.json(results.data);
});

router.post('/register', bodyParser.json(), async function(req, res) {
  const results = await httpClient.post(
    'RegisterUser',
    req.body,
    handleAuthHeaders(req),
  );
  res.json(results.data);
});

router.get('/logout', function(req, res) {
  const cookies = new Cookies(req, res);
  const expires = new Date();
  expires.setFullYear(2099);
  cookies.set(
    'session_id',
    null
  );
  res.send();
});

router.post('/login', bodyParser.json(), async function(req, res) {
  const results = await httpClient.post(
    'login',
    req.body,
    handleAuthHeaders(req),
  );
  const result = results.data;
  if (result.data && result.data.token) {
    const cookies = new Cookies(req, res);
    const expires = new Date();
    expires.setFullYear(2099);
    cookies.set(
      'session_id',
      result.data.token,
      { httpOnly: true, expires }
    );
  }
  res.json(result);
});

router.get('/results/:provider/:race/:event', async function(req, res) {
  if (req.params.provider && req.params.race && req.params.event) {
    const results = await httpClient.get(
      `RaceResults?RaceProviderURL=${req.params.provider}&RaceURL=${req.params.race}&RaceEventURL=${req.params.event}`,
    );
    res.json(results.data.data);
  } else {
    res.send();
  }
});

router.get('/race/:provider/:race/:event', async function(req, res) {
  if (req.params.provider && req.params.race) {
    const results = await httpClient.get(
      `RaceDetails?RaceProviderURL=${req.params.provider}&RaceURL=${req.params.race}&eventURL=${req.params.event}`,
    );
    res.json(results.data.data);
  } else {
    res.send();
  }
});

module.exports = router;
