import moment from 'moment';

export const parseRawRace = function(rawRace) {
  const isOneDay = rawRace.DateStart === rawRace.DateEnd;
  const raceStart = moment(rawRace.DateStart);
  const startMonth = raceStart.month();
  const startYear = raceStart.year();
  const raceEnd = moment(rawRace.DateEnd);
  const endMonth = raceEnd.month();
  const endYear = raceEnd.year();
  const fromPart = `${raceStart.format('MMMM')} ${raceStart.format('Do')}${startYear !== endYear ? ', ' + startYear : ''}`;
  const toPart = `${startMonth !== endMonth ? endMonth.format('MMM') : ''} ${raceEnd.format('Do')}${', ' + endYear}`;
  const dateString = isOneDay ? `${raceStart.format('MMMM Do, YYYY')}` : `${fromPart} - ${toPart}`;

  const event = rawRace.Events[0];
  const leg = rawRace.Events[0]?.Legs[0];
  const level = rawRace.Events[0]?.Levels[0];
  const plans = rawRace.Events[0]?.Levels[0]?.Plans;

  let dateDifferenceValue = 'Closed';
  let dateDifferenceName = 'Registration';
  let raceState = 'closed';
  const today = moment();
  const signupStart = moment(rawRace.SignupOpen);
  const signupEnd = moment(rawRace.SignupCutOff);

  const diffStart = today.diff(signupStart, 'd');
  const diffEnd = today.diff(signupEnd, 'd');
  const planDate = moment(level.PlanStartDate);

  if (today < signupStart) {
    if (diffStart === 1) {
      dateDifferenceValue = 'Tomorrow';
      dateDifferenceName = 'Registration Opens';
      raceState = 'waiting';
    } else if (diffStart < 0) {
      const days = Math.abs(diffStart);
      dateDifferenceValue = `${days} day${days > 1 ? 's' : ''}`;
      dateDifferenceName = 'Until Registration Opens';
      raceState = 'waiting';
    }
  } else if (today > signupStart && today < signupEnd) {
    if (diffEnd === 1) {
      dateDifferenceValue = 'Tomorrow';
      dateDifferenceName = 'Registration Closes';
      raceState = 'open';
    } else {
      const days = Math.abs(diffEnd);
      dateDifferenceValue = `${days} day${days > 1 ? 's' : ''}`;
      dateDifferenceName = 'Until Registration Closes';
      raceState = 'open';
    }
  }

  if (today > raceEnd) {
    dateDifferenceValue = 'Finished';
    dateDifferenceName = 'Race has finished';
    raceState = 'finished';
  }

  let raceType = 'Other';
  switch (leg.ActivityTypeID) {
    case 1:
      raceType = 'Run';
      break;
    case 2:
      raceType = 'Bike';
      break;
    case 3:
      raceType = 'Swim';
      break;
  }

  const sponsors = rawRace.Sponsors.map(rawSponsor => ({
    key: rawSponsor.RaceSponsorKey,
    image: rawSponsor.LogoURL,
    link: rawSponsor.Website,
  }));

  let distance = `${leg.Distance}${leg.DistanceType}`;
  distance = distance === '13.1mi' ? 'Half Marathon' : (distance === '26.2mi' ? 'Marathon' : distance);
  distance = distance === '21.097km' ? 'Half Marathon' : (distance === '42.195km' ? 'Marathon' : distance);

  const race = {
    key: rawRace.RaceKey,
    eventKey: event.RaceEventKey,
    levelKey: level.RaceLevelKey,
    planKey: plans[0] ? plans[0].PlanKey : '',
    image: rawRace.HeroURL,
    event,
    leg,
    plans,
    isOneDay,
    logo: rawRace.LogoURL,
    url: rawRace.URL,
    name: rawRace.Name,
    description: rawRace.Description,
    socialDescription: rawRace.SocialDescription,
    distance: distance,
    distanceString: event.DistanceDescription,
    type: raceType,
    price: level ? level.Price : 0,
    state: raceState,
    dateDifferenceValue,
    dateDifferenceName,
    dateString,
    dateFrom: fromPart,
    dateTo: toPart,
    planDate: planDate.isValid() ? planDate.format('MMM Do') : null,
    raceDate: moment(leg.DefaultDate).format('dddd, MMM Do'),
    sponsors,
    facebook: rawRace.Facebook,
    instagram: rawRace.Instagram,
    twitter: rawRace.Twitter,
    linkedin: rawRace.LinkedIn,
  };

  return race;
};
